FROM florenttorregrosa/drupal-php:8.3-apache

ARG DRUPAL_ORG_CLI_VERSION=latest
ARG NODEJS_VERSION=18

# Get Composer from official image.
COPY --from=composer:2 /usr/bin/composer /usr/bin/composer

# Allows to disable or enable Xdebug depending on environment variables.
COPY bin/configure-xdebug /usr/local/bin/configure-xdebug

RUN chmod +x /usr/local/bin/configure-xdebug \
    # Add some Bash aliases.
    && echo 'alias ll="ls -l"' >> $HOME/.bashrc \
    && echo 'alias lll="ls -al"' >> $HOME/.bashrc \
    # Add bins from Composer's global and project vendor in PATH.
    && echo 'PATH="$HOME/.composer/vendor/bin:/project/bin:/project/vendor/bin:$PATH"' >> $HOME/.bashrc \
    # Avoid Git problem with GrumPHP.
    && git config --global --add safe.directory /project

# Install Drupal.org CLI.
RUN curl -sSL "https://github.com/mglaman/drupalorg-cli/releases/${DRUPAL_ORG_CLI_VERSION}/download/drupalorg.phar" -o /usr/local/bin/drupalorg \
    && chmod +x /usr/local/bin/drupalorg

# Install Ngrok.
RUN curl -sSL https://bin.equinox.io/c/bNyj1mQVY4c/ngrok-v3-stable-linux-amd64.tgz -o ngrok-v3-stable-linux-amd64.tgz \
    && tar -xf ngrok-v3-stable-linux-amd64.tgz \
    && rm ngrok-v3-stable-linux-amd64.tgz \
    && mv ngrok /usr/local/bin/ \
    && chmod +x /usr/local/bin/ngrok \
    && ngrok update

# Install PHIVE and some PHP tools.
RUN curl -sSL https://phar.io/releases/phive.phar -o phive.phar \
    && curl -sSL https://phar.io/releases/phive.phar.asc -o phive.phar.asc \
    && gpg --keyserver hkps://keys.openpgp.org --recv-keys 0x9D8A98B29B2D5D79 \
    && gpg --verify phive.phar.asc phive.phar \
    && chmod +x phive.phar \
    && mv phive.phar /usr/local/bin/phive \
    && rm phive.phar.asc \
    # Install latest version of Composer Normalize. Can not be installed with
    # project sources because of conflict with drupal-paranoia.
    && phive --no-progress install --global --trust-gpg-keys C00543248C87FB13 composer-normalize

# Install Xdebug and Make.
RUN apt-get update \
    && apt-get install -y $PHPIZE_DEPS \
    && pecl install \
        xdebug \
    && docker-php-ext-enable \
        xdebug \
    && apt-get remove -y $PHPIZE_DEPS \
    # Ensure make is installed.
    && apt-get install -y \
        make \
    && pecl clear-cache \
    && apt-get autoremove -y -q \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man /tmp/* \
    # Disable Xdebug by default.
    && mv /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.disabled

# Install NodeJS and Yarn.
RUN curl -sSL "https://deb.nodesource.com/setup_${NODEJS_VERSION}.x" | bash - \
    && apt-get install -y \
        nodejs \
    && npm install --global \
        yarn \
    && npm cache clean --force \
    && apt-get autoremove -y -q \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man /tmp/*

WORKDIR /project

# Exec configure Xdebug helper script before running the parent CMD script.
CMD ["configure-xdebug"]
